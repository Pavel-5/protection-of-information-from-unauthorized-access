# Лабораторная работа №1

## Цель

1. Создать собственный docker
2. Запустить ранее созданный docker

## Исходные данные

1. Ноутбук
2. docker
3. dockerhub.com

## Варианты решения задачи

### Вариант 1

1. Создание файла Dockerfile
```
FROM ubuntu:latest
RUN apt-get update
CMD ["cat", "/var/log/alternatives.log"]
```
2. Создание docker по файлу Dockerfile
```
docker build -t l1 .
```
3. Запустить docker
```
docker run -i -t l1
```
### Вариант 2

1. Создание скрипта bash myScript.sh
```
#!/bin/bash
cd /var/log
cat alternatives.log
```
2. Создание файла Dockerfile
```
FROM ubuntu:latest
RUN apt-get update
ADD myScript.sh /myScript.sh
CMD ["/myScript.sh"]
```
3. Создание docker по файлу Dockerfile
```
docker build -t l1 .
```
4. Запустить docker
```
docker run -i -t l1
```
## Общий план выполнения

1. Создать файлы, которые нужны для docker
2. Создать docker
3. Запустить docker

## Содержание ЛР

### Шаг 1

Создать docker

### Шаг 2

Запустить docker

### Шаг 3

Сформировать отчет

### Шаг 4

Сохранить всё на удаленном репозитории

## Оценка результата

В итоге в терминал выводится содержимое файла alternatives.log

## Вывод

В ходе лабораторной работы, мы научились создавать и запускать docker.
